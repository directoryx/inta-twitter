<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Twitter;
use DB;
use Carbon\Carbon;
use App\Prosa;

class TwitterMod extends Model
{
    public function getTweet($string){
        $data = Twitter::getSearch(['result_type'=>'latest','q'=> $string.' -filter:retweets -filter:links','count' => 100, 'format' => 'array','tweet_mode'=> 'extended','retweeted'=>false]);
        $data = $data["statuses"];
        return $data;
    }

    public function getall(){
        $i = 0;
        $start1 = microtime(true); 
        $data = Twitter::getSearch(['result_type'=>'latest','q'=> 'prabowo sandiaga -filter:retweets -filter:links','count' => 50, 'format' => 'array','tweet_mode'=> 'extended','retweeted'=>false]);
        $data = $data["statuses"];
        $mytime = Carbon::now();
        $prosa = new Prosa();
        foreach ($data as $key => $value) {
            $analystsen = $prosa->analystprosa($value['full_text']);
            DB::table('final')->insert(
                [
                    'idstr' => $value['id_str'],
                    'tweet' => $value['full_text'],
                    'username'=>$value["user"]["screen_name"],
                    'polarity'=>$analystsen["sentiment"],
                    'confidence'=>$analystsen["confidence"],
                    'platform'=>$value["source"],
                    'tanggaltweet'=>$value["created_at"],
                    'paslon'=>"prabowo sandiaga",
                    'execute_date'=>$mytime
                ]
            );
        }
        $end1 = number_format(microtime(true) - $start1, 10). " seconds.";
        DB::table('time')->insert(
            [
                'paslon' => "prabowo sandiaga",
                'time'=> $end1,
                'lastupdate'=>$mytime
            ]
        );
        $start2 = microtime(true); 
        $data = Twitter::getSearch(['result_type'=>'latest','q'=> 'jokowi maruf -filter:retweets -filter:links','count' => 50, 'format' => 'array','tweet_mode'=> 'extended','retweeted'=>false]);
        $data = $data["statuses"];
        foreach ($data as $key => $value) {
            //DB::table('tweet')->insert(
            //    ['tweet' => $value['full_text'], 'tanggal' => $value["created_at"],'execute_date'=>$mytime,'idtweet'=>$value["id"],'iduser'=>$value["user"]["id_str"],'username'=>$value["user"]["screen_name"],'namaakun'=>$value["user"]["name"],'platform'=>$value["source"],'paslon'=>"jokowi maruf"]
            //);
            $analystsen = $prosa->analystprosa($value['full_text']);
            DB::table('final')->insert(
                [
                    'idstr' => $value['id_str'],
                    'tweet' => $value['full_text'],
                    'username'=>$value["user"]["screen_name"],
                    'polarity'=>$analystsen["sentiment"],
                    'tanggaltweet'=>$value["created_at"],
                    'confidence'=>$analystsen["confidence"],
                    'platform'=>$value["source"],
                    'paslon'=>"jokowi maruf",
                    'execute_date'=>$mytime
                ]
            );
        }
        $end2 = number_format(microtime(true) - $start2, 10). " seconds.";
        DB::table('time')->insert(
            [
                'paslon' => "jokowi maruf",
                'time'=> $end2,
                'lastupdate'=>$mytime
            ]
        );
        DB::table('setting')->insert(
            ['lastupdate'=> $mytime] 
        );
        return true;
        //$data1 = Twitter::getSearch(['result_type'=>'latest','q'=> 'jokowi maruf -filter:retweets -filter:links','count' => 100, 'format' => 'array','tweet_mode'=> 'extended','retweeted'=>false]);
        
    }

    public function getlastupdate(){
        return DB::table('setting')->orderBy('id', 'desc')->first();
    }

    public function getcurrentcountmostuserpra(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        $users = DB::table('final')
                     ->select(DB::raw('count(*) as count, username'))
                     ->where('execute_date', '=', $lastexecute)
                     ->where('paslon', '=', 'prabowo sandiaga')
                     ->groupBy('username')
                     ->orderBy("count",'desc')
                     ->limit(3)
                     ->get();
        return $users;
    }


    public function getcurrentcountmostuserjok(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        $users = DB::table('final')
                     ->select(DB::raw('count(*) as count, username'))
                     ->where('execute_date', '=', $lastexecute)
                     ->where('paslon', '=', 'jokowi maruf')
                     ->groupBy('username')
                     ->orderBy("count",'desc')
                     ->limit(3)
                     ->get();
        return $users;
    }

    public function getcurrentcountplatjok(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        $users = DB::table('final')
                     ->select(DB::raw('count(*) as count, platform'))
                     ->where('execute_date', '=', $lastexecute)
                     ->where('paslon', '=', 'jokowi maruf')
                     ->groupBy('platform')
                     ->orderBy("count",'desc')
                     ->limit(3)
                     ->get();
        return $users;
    }

    public function getcurrentcountsentimentjok(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        $users = DB::table('final')
                     ->select(DB::raw('count(*) as count, polarity'))
                     ->where('execute_date', '=', $lastexecute)
                     ->where('paslon', '=', 'jokowi maruf')
                     ->groupBy('polarity')
                     ->orderBy("count",'desc')
                     ->limit(3)
                     ->get();
        return $users;
    }

    public function getcurrentcountsentimentpra(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        $users = DB::table('final')
                     ->select(DB::raw('count(*) as count, polarity'))
                     ->where('execute_date', '=', $lastexecute)
                     ->where('paslon', '=', 'prabowo sandiaga')
                     ->groupBy('polarity')
                     ->orderBy("count",'desc')
                     ->limit(3)
                     ->get();
        return $users;
    }

    public function getcurrentcountplatpra(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        $users = DB::table('final')
                     ->select(DB::raw('count(*) as count, platform'))
                     ->where('execute_date', '=', $lastexecute)
                     ->where('paslon', '=', 'prabowo sandiaga')
                     ->groupBy('platform')
                     ->orderBy("count",'desc')
                     ->limit(3)
                     ->get();
        return $users;
    }

    public function gettweetpra(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        
        $users = DB::table('final')
                    ->select('tweet','username','platform','id','polarity','tanggaltweet','idstr')
                     ->where('execute_date', '=', $lastexecute)
                     ->where('paslon', '=', 'prabowo sandiaga')
                     ->get();
        return $users;
    }

    public function gettweetjok(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        
        $users = DB::table('final')
                     ->select('tweet','username','platform','id','polarity','tanggaltweet','idstr')
                     ->where('execute_date', '=', $lastexecute)
                     ->where('paslon', '=', 'jokowi maruf')
                     ->get();
        //dd($users);
        return $users;
    }

    public function gettweetjok2(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        
        $users = DB::table('final')
                     
                     ->where('execute_date', '=', $lastexecute)
                     ->where('paslon', '=', 'jokowi maruf')
                     ->get();
        //dd($users);
        return $users;
    }


    public function getexetimepra(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        
        $users = DB::table('time')
                     
                    ->where('lastupdate', '=', $lastexecute)
                     ->where('paslon', '=', 'prabowo sandiaga')
                     ->first();

        return $users;
        

    }

    public function getexetimejok(){
        $now = $this->getlastupdate();
        $lastexecute = $now->lastupdate;
        
        $users = DB::table('time')
                     
                     ->where('lastupdate', '=', $lastexecute)
                     ->where('paslon', '=', 'jokowi maruf')
                     ->first();
        return $users;
    }

    public function totalexetime(){
        $pra = $this->getexetimepra();
        $pratime = $pra->time;
        $search = 'seconds.' ;
        $trimmed = str_replace($search, '', $pratime) ;
        $jok = $this->getexetimejok();
        $joktime = $jok->time;
        $search = 'seconds.' ;
        $trimmed1 = str_replace($search, '', $joktime) ;
        $total = (float)$trimmed+(float)$trimmed1;
        return $total;
    }
}
