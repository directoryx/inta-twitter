<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prosa extends Model
{
    

    public function analystprosa($text){
        // create curl resource 
        $ch = curl_init(); 

        //$apikey = "p56PKmYtyQBNefyzI0Y8TCl1UgEDCnvCbABjmyOT";
        $apikey = env("API_PROSA_KEY", "yourkey");

        // set url 
        curl_setopt($ch, CURLOPT_URL, "https://api.prosa.ai/v1/sentiments"); 

        $data = array("text" => $text);                                                                    
        $data_string = json_encode($data);                                                                                   

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-type: application/json',
            'x-api-key: '.$apikey
        ));

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);  
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 400); //timeout in seconds


        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch);  
        
        $json = json_decode($output, true);

        return $json;
    }
}
