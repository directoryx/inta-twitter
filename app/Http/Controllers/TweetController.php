<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tweet;
use App\StringCompareJaroWinkler;

class TweetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $tweet = Tweet::find($id);
        $comparison = new \Atomescrochus\StringSimilarities\Compare();
        $tokenizerFactory  = new \Sastrawi\Tokenizer\TokenizerFactory();
        $tokenizer = $tokenizerFactory->createDefaultTokenizer();
        $tokens = $tokenizer->tokenize($tweet->tweet);
        $i = 0;
        $arr = array();
        $arr1 = array();
        
        //print_r($tokens);
        //$jaroWinkler1 = $comparison->jaroWinkler('JOKOWI', $tokens[12]);
        //echo $jaroWinkler1;
        if($tweet->paslon == "jokowi maruf"){
            foreach ($tokens as $token){
                //echo $i."<br/>";
                
                $start1 = microtime(true); 
                $jaroWinkler1 = $comparison->jaroWinkler('jokowi', strtolower($tokens[$i]));
                $end1 = number_format(microtime(true) - $start1, 10). " seconds.";
                $start2 = microtime(true); 
                $levenshtein1 = $comparison->levenshtein('jokowi', strtolower($tokens[$i]));
                $end2 = number_format(microtime(true) - $start2, 10). " seconds.";
                if($jaroWinkler1 >= 0.6 && $levenshtein1 <= 4){
                    $arr[] = array('jokowi',strtolower($tokens[$i]),$jaroWinkler1,$end1,$levenshtein1,$end2);
                }
                $start3 = microtime(true); 
                $jaroWinkler1 = $comparison->jaroWinkler('maruf', strtolower($tokens[$i]));
                $end3 = number_format(microtime(true) - $start3, 10). " seconds.";
                $start4 = microtime(true); 
                $levenshtein1 = $comparison->levenshtein('maruf', strtolower($tokens[$i]));
                $end4 = number_format(microtime(true) - $start4, 10). " seconds.";
                if($jaroWinkler1 >= 0.6 && $levenshtein1 <= 4){
                    $arr1[] = array('maruf',strtolower($tokens[$i]),$jaroWinkler1,$end3,$levenshtein1,$end4);
                }
                $i++;
            }
            //dd($arr);
            return view('tweetdetail')->with('token',$tokens)->with('tweet',$tweet)->with('arr',$arr)->with('arr1',$arr1);
        } else {
            foreach ($tokens as $token){
                $start1 = microtime(true); 
                $jaroWinkler1 = $comparison->jaroWinkler('prabowo', strtolower($tokens[$i]));
                $end1 = number_format(microtime(true) - $start1, 10). " seconds.";
                $start2 = microtime(true); 
                $levenshtein1 = $comparison->levenshtein('prabowo', strtolower($tokens[$i]));
                $end2 = number_format(microtime(true) - $start2, 10). " seconds.";
                if($jaroWinkler1 >= 0.6 && $levenshtein1 <= 4){
                    $arr[] = array('prabowo',strtolower($tokens[$i]),$jaroWinkler1,$end1,$levenshtein1,$end2);
                }
                $start3 = microtime(true); 
                $jaroWinkler1 = $comparison->jaroWinkler('sandiaga', strtolower($tokens[$i]));
                $end3 = number_format(microtime(true) - $start3, 10). " seconds.";
                $start4 = microtime(true); 
                $levenshtein1 = $comparison->levenshtein('sandiaga', strtolower($tokens[$i]));
                $end4 = number_format(microtime(true) - $start4, 10). " seconds.";
                if($jaroWinkler1 >= 0.6 && $levenshtein1 <= 4){
                    $arr1[] = array('sandiaga',strtolower($tokens[$i]),$jaroWinkler1,$end3,$levenshtein1,$end4);
                }
                $i++;
            }
            return view('tweetdetail2')->with('token',$tokens)->with('tweet',$tweet)->with('arr',$arr)->with('arr1',$arr1);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
