<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TwitterMod;
use App\Exports\FinalExport;
use DB;
use Excel;

class ExcelController extends Controller
{
    public function jokowi(){
        $twittermod = new TwitterMod();
        $now = $twittermod->getlastupdate();
        $lastexecute = $now->lastupdate;
        $filename="jokowi-".$lastexecute.".xlsx";
        
        return (new FinalExport($lastexecute,"jokowi maruf"))->download($filename);
    }

    public function prabowo(){
        $twittermod = new TwitterMod();
        $now = $twittermod->getlastupdate();
        $lastexecute = $now->lastupdate;
        $filename="prabowo-".$lastexecute.".xlsx";
        
        return (new FinalExport($lastexecute,"prabowo sandiaga"))->download($filename);
    }

}
