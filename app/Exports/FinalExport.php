<?php

namespace App\Exports;

use App\Finals;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
 

class FinalExport implements FromQuery,WithHeadings
{
    use Exportable;

    public function __construct($year,$paslon)
    {
        $this->year = $year;
        $this->paslon = $paslon;
    }

    public function query()
    {
        return Finals::query()->where('execute_date', $this->year)->where('paslon',$this->paslon);
    }

    public function headings(): array
    {
        return [
            'ID',
            'ID Tweet',
            'Tweet',
            'Username',
            'Tanggal Tweet',
            'Polarity',
            'Confidence',
            'Platforms',
            'execute_date',
            'Paslon'
        ];
    }
}