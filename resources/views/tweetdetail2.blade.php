
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Sentiment Analyst : Pasangan Calon Pemilu 2019</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" >
    
    <!-- Custom styles for this template -->
    <link href="pricing.css" rel="stylesheet">
  </head>

  <body>

    

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Tweet Detail</h1>
      <p class="lead">{{$tweet->tweet}}</p>
    </div>

    <div class="container">
      <!--
      <div class="card-deck mb-3 text-center">
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Free</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$0 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>10 users included</li>
              <li>2 GB of storage</li>
              <li>Email support</li>
              <li>Help center access</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary">Sign up for free</button>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Pro</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$15 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>20 users included</li>
              <li>10 GB of storage</li>
              <li>Priority email support</li>
              <li>Help center access</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Get started</button>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Enterprise</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$29 <small class="text-muted">/ mo</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>30 users included</li>
              <li>15 GB of storage</li>
              <li>Phone and email support</li>
              <li>Help center access</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Contact us</button>
          </div>
        </div>
      </div>
      -->
      <br/>
      <br/>
      <br/>
      <center><p class="lead">Tokenizer</p></center>
      <br/>
      <div class="row">
            <div class="col-lg-12">
              <table class="table table-bordered" id="myTable">
                  <thead>
                      <tr>
                          <th width="50px">No</th>
                          <th>Token</th>
                      </tr>
                  </thead>
                  <tbody>
                        @php($i=0)
                      @if(!empty($token))
                          @foreach($token as $key => $value)
                              <tr>
                                  <td>{{++$key}}</td>
                                  <td>{{strtolower($token[$i])}}</td>
                              </tr>
                              @php($i++)
                          @endforeach
                      @else
                          <tr>
                              <td colspan="6">There are no data.</td>
                          </tr>
                      @endif
                  </tbody>
              </table>
            </div>
           
            <div class="col-lg-12">
            <br/>
            <br/>
            <br/>
            <center><p class="lead">Similarity - Prabowo</p></center>
              <table class="table table-bordered" id="myTable1">
                  <thead>
                      <tr>
                          <th width="50px">No</th>
                          <th>Kata Kunci</th>
                          <th>Token</th>
                          <th>Jaro Winkler</th>
                          <th>Execute Time(JW)</th>
                          <th>Levenstein</th>
                          <th>Execute Time(LS)</th>
                      </tr>
                  </thead>
                  <tbody>
                        @php($i=0)
                      @if(!empty($arr))
                          @foreach($arr as $value)
                              <tr>
                                  <td>1</td>
                                  <td>{{$value[0]}}</td>
                                  <td>{{$value[1]}}</td>
                                  <td>{{$value[2]}}</td>
                                  <td>{{$value[3]}}</td>
                                  <td>{{$value[4]}}</td>
                                  <td>{{$value[5]}}</td>
                              </tr>
                              @php($i++)
                          @endforeach
                      @else
                          <tr>
                              <td colspan="6">There are no data.</td>
                          </tr>
                      @endif
                  </tbody>
              </table>
            </div>

            <div class="col-lg-12">
            <br/>
            <br/>
            <br/>
            <center><p class="lead">Similarity - Sandiaga</p></center>
              <table class="table table-bordered" id="myTable2">
                  <thead>
                      <tr>
                          <th width="50px">No</th>
                          <th>Kata Kunci</th>
                          <th>Token</th>
                          <th>Jaro Winkler</th>
                          <th>Execute Time(JW)</th>
                          <th>Levenstein</th>
                          <th>Execute Time(LS)</th>
                      </tr>
                  </thead>
                  <tbody>
                        @php($i=0)
                      @if(!empty($arr1))
                          @foreach($arr1 as $value)
                              <tr>
                                  <td>1</td>
                                  <td>{{$value[0]}}</td>
                                  <td>{{$value[1]}}</td>
                                  <td>{{$value[2]}}</td>
                                  <td>{{$value[3]}}</td>
                                  <td>{{$value[4]}}</td>
                                  <td>{{$value[5]}}</td>
                              </tr>
                              @php($i++)
                          @endforeach
                      @else
                          <tr>
                              <td colspan="6">There are no data.</td>
                          </tr>
                      @endif
                  </tbody>
              </table>
            </div>
        </div>
    


      <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md ">
            
            <small class="d-block mb-3 text-muted">DirectoryX &copy; 2018</small>
          </div>
          
        </div>
      </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js" integrity="sha256-JG6hsuMjFnQ2spWq0UiaDRJBaarzhFbUxiUTxQDA9Lk=" crossorigin="anonymous"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable();
            $('#myTable1').DataTable();
            $('#myTable2').DataTable();
        } );
	</script>
  </body>
</html>
