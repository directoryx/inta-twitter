@extends('layouts.app')

@section('content')
<br/>
<br/>
<div class="container">
    <div class="row justify-content-center">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <center>Selamat Datang di Admin Panel Sentiment Analyst</center>
                </div>
            </div>
    </div>
</div>
@endsection
