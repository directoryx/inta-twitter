<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/
Auth::routes();
Route::get('/refresh', 'HomeController@refresh');
Route::get('/testanalyst', 'TestController@testp1');
Route::get('/', 'TwitterController@index');
Route::get('/1', 'TwitterController@jowoki');
Route::get('/1/excel', 'ExcelController@jokowi');
Route::get('/2/excel', 'ExcelController@prabowo');
Route::get('/2', 'TwitterController@prabowo');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test','TestController@index')->name('test');
Route::get('/tweet/{id}/detail','TweetController@index')->name('detailtweet');
Route::get('/realtweet','TestController@realtweet')->name('realtweet');
Route::get('/dos','TestController@time')->name('time');
Route::get('/tweetjok','TestController@tweetjok')->name('tweet');
Route::get('/testing120', 'TestController@testp1');
